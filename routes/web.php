<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Hospital\DoctorController;
use App\Http\Controllers\Hospital\PatientController;
use App\Http\Controllers\Hospital\UserController;
use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


use App\Models\Invoice;
use App\Notifications\InvoicePaid;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home'); // Profile

Route::get('/logout', [HomeController::class, 'logout'])->name('logout');

Route::get('/add/doctor', [DoctorController::class, 'addDoctor'])->name('add.doctor');
Route::post('/save/doctor', [DoctorController::class, 'saveDoctor'])->name('save.doctor');

Route::get('/add/patient', [PatientController::class, 'addPatient'])->name('add.patient');
Route::post('/save/patient', [PatientController::class, 'savePatient'])->name('save.patient');







Route::get('/patient/appointments', [PatientController::class, 'index'])->name('patient.appointments');
Route::get('/appointment/{id}', [PatientController::class, 'appointment'])->name('appointment');

Route::get('/patient/make/appointment', [PatientController::class, 'create'])->name('patient.make.appointment');
Route::post('/patient/save/appointment', [PatientController::class, 'store'])->name('patient.save.appointment');

Route::get('/patient/cancel/appointment/{patient_id}/{id}', [PatientController::class, 'confirmCancelAppointment'])->name('patient.cancel.appointment');
Route::get('/patient/delete/appointment/{patient_id}/{id}', [PatientController::class, 'deleteAppointment'])->name('patient.delete.appointment');

Route::get('email/send', [MailController::class, 'send'])->name('email.send');


Route::get('patients', [DoctorController::class, 'index'])->name('patients');

Route::get('/doctor/write/card', [DoctorController::class, 'create'])->name('write.to.card');
Route::post('/doctor/save/card', [DoctorController::class, 'store'])->name('doctor.save.card');


//Route::get('/test', [PatientController::class, 'test'])->name('test');

Route::middleware('ajax')
    ->prefix('/api')
    ->group(function(Router $router) {
        $router->get('users', [UserController::class, 'index']);
        $router->post('add/user', [UserController::class, 'addUser']);
        $router->post('edit/{user}', [UserController::class, 'updateUser']);
        $router->post('delete/{user}', [UserController::class, 'deleteUser']);


        $router->post('delete/appointment/{id}', [PatientController::class, 'deleteApp']);

        $router->get('appointments/{id}', [PatientController::class, 'appointmentVue']);

        $router->post('write/history', [DoctorController::class, 'writeHistory']);
    });

Route::get('users', [UserController::class, 'users'])->name('users');

Route::get('appointments/{id}', [PatientController::class, 'appointmentsShow'])->name('appointments.show');
