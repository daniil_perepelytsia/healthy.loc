<?php

namespace App\Console\Commands;

use App\Models\Hospital\Patient;
use Illuminate\Console\Command;

class MakeAppointment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:appointment {patient_id} {doctor_id} {date} {time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make an appointment with a doctor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Patient $patientId)
    {
        $data = $this->arguments();

        $this->info('Parameters: ' . implode(', ', $data));

        $patient = $patientId->find($data['patient_id']);
        $patient->doctors()->attach($data['doctor_id'], ['appointment_time' => $data['date'] . ' ' . $data['time'] . ':00']);
    }
}
