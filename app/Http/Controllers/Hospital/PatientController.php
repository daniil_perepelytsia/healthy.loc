<?php

namespace App\Http\Controllers\Hospital;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientDoctorRequest;
use App\Http\Requests\UserCreateRequest;
use App\Mail\TestMail;
use App\Models\Hospital\Doctor;
use App\Models\Hospital\Patient;
use App\Models\Hospital\PatientDoctor;
use App\Models\User;
use App\Notifications\InvoicePaid;
use App\Repositories\DoctorRepository;
use App\Repositories\PatientRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PhpParser\Comment\Doc;

class PatientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * SHOW APPOINTMENTS
     *
     * @return Application|Factory|View|Response
     */
    public function index(PatientRepository $patientRepository)
    {
        $app = Patient::with(['doctors', 'users'])->paginate(10);

        return view('hospital/patient_appointments', ['app' => $app]);
    }

    public function appointment($id, DoctorRepository $doctorRepository){
        $appointment = Patient::with('doctors')->find($id);

        return view('hospital/appointment', ['appointment' => $appointment->doctors]);
    }


    /**
     * CREATE APPOINTMENT
     *
     * @param DoctorRepository $doctorRepository
     * @return Application|Factory|View|Response
     */
    public function create(DoctorRepository $doctorRepository)
    {
        $doctorsData = $doctorRepository->getDoctors();

        $users = User::select('id', 'name', 'email')->get();

        return view('hospital/make_appointment', ['doctorsData' => $doctorsData, 'users' => $users]);
    }



    /**
     * SAVE APPOINTMENT
     *
     * @param PatientDoctorRequest $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(PatientDoctorRequest $request)
    {
        $patient = Patient::where('user_id', $request->patient)->first();
        if(!isset($patient)) {
            $patient = new Patient(['user_id' => $request->patient]);
            $patient->save();
        }
        if($request->patient !== $request->doctor) {
            $patient->doctors()->attach($request->doctor, ['appointment_time' => $request->datetime]);
        }
        else{
            return back()->withErrors(['msg' => 'Error! You have selected the same people as doctor and patient!'])->withInput();
        }

//        $doctor = Doctor::where('user_id', $request->doctor)->first(); // For sending email
//        Notification::send($patient, new InvoicePaid('Dear ' . $patient->name . '. You just made an appointment with a doctor ' . $doctor->name . PHP_EOL .
//                '. Appointment time - ' . $request->date . ' on ' . $request->time));
//        Mail::mailer('cruzywork@gmail.com')
//            ->to($patient)
//            ->send(new TestMail('Dear ' . $patient->name . '. You just made an appointment with a doctor ' . $doctor->name . PHP_EOL .
//                '. Appointment time - ' . $request->date . ' on ' . $request->time));
        return redirect('/');
    }




    /**
     * CONFIRM CANCELLATION APPOINTMENT
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function confirmCancelAppointment($patientId, $id){
        return view('hospital/cancel_appointment', ['patient_id' => $patientId, 'id' => $id]);
    }


    /**
     * DELETE APPOINTMENT
     *
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function deleteAppointment($patientId, $id){

        $patient = Patient::find($patientId);
        $patient->doctors()->wherePivot('id', $id)->detach();

        return redirect('patient/appointments');
    }


    public function addPatient(){

        return view('hospital/add_patient');
    }

    public function savePatient(Request $request){

        $user = User::where('email', $request->email)->first();
        if(isset($user)){
            if($user->name == $request->name){

                $patient = new Patient(['user_id' => $user->id]);
                $patient->save();
            }
            else{
                dd('Данные не совпадают!!!');
            }
        }
        else{
            dd('Пользователь не найден! Сначала зарегистрируйте его!');
        }
        return redirect('home');
    }


    public function test(Request $request)
    {
        return view('test');
    }



    public function appointmentVue($id){
        $appointment = Patient::with('doctors')->find($id);
        return response()->json($appointment->doctors);
    }

    public function appointmentsShow($id){
        return \view('hospital/show_appointments', ['id' => $id]);
    }

    public function deleteApp(Request $request){

        $patient = Patient::find($request->patient_id);
        $patient->doctors()->wherePivot('id', $request->id)->detach();
        dd($patient);
    }

}
