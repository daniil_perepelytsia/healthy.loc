<?php

namespace App\Http\Controllers\Hospital;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientHistoriesRequest;
use App\Models\Hospital\Doctor;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Comment\Doc;
use Symfony\Component\Console\Helper\Table;

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $data = Doctor::with('patients')->find(Auth::id());

        return view('hospital/patients', ['patients' => $data->patients]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        $patients = User::where('position', 0)->get();

        return view('hospital/write_to_card', ['patients' => $patients]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(PatientHistoriesRequest $request)
    {
        $doctor = Doctor::find(Auth::id());

        $doctor->patients()->attach($request->patient, ['diagnosis' => $request->diagnosis, 'treatment' => $request->treatment]);

        return redirect('/');
    }

    public function addDoctor(){

        $specializations = [
            'Allergy and immunology',
            'Anesthesiology',
            'Dermatology',
            'Diagnostic radiology',
            'Emergency medicine',
            'Family medicine',
            'Internal medicine',
            'Medical genetics',
            'Neurology',
            'Nuclear medicine',
            'Obstetrics and gynecology',
            'Ophthalmology',
            'Pathology',
            'Pediatrics',
            'Physical medicine and rehabilitation',
            'Preventive medicine',
            'Psychiatry',
            'Radiation oncology',
            'Surgery',
            'Urology'
        ];

        return view('hospital/add_doctor', compact('specializations'));
    }

    public function saveDoctor(Request $request){

        $user = User::where('email', $request->email)->first();
        if(isset($user)){
            if($user->name == $request->name){
                $data = [
                    'user_id' => $user->id,
                    'specialization' => $request->specialization
                    ];
                $doctor = new Doctor($data);
                $doctor->save();
            }
            else{
                dd('Данные не совпадают!!!');
            }
        }
        else{
            dd('Пользователь не найден! Сначала зарегистрируйте его!');
        }
        return redirect('home');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        dd(__METHOD__);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        dd(__METHOD__);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        dd(__METHOD__);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        dd(__METHOD__);
    }


    public function writeHistory(Request $request){
//        dd($request->all());
        DB::table('patient_histories')->insert(
            [
                'patient_doctor_id' => $request->patient_doctor_id,
                'diagnosis' => $request->diagnosis,
                'treatment' => $request->treatment
            ]
        );

    }
}
