<?php


namespace App\Http\Controllers\Hospital;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientDoctorRequest;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Mail\TestMail;
use App\Models\Hospital\Doctor;
use App\Models\Hospital\Patient;
use App\Models\Hospital\PatientDoctor;
use App\Models\User;
use App\Notifications\InvoicePaid;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PhpParser\Comment\Doc;

class UserController extends Controller
{
    public function index()
    {
        $data = User::orderBy('name')->get(['id', 'name', 'email']);

        return response()->json($data);
    }

    public function users()
    {
        return view('test');
    }

    public function addUser(Request $request){

        $data[] = $request->data;

        $user = new User($request->data);
        $user->save();
        $id = User::select('id')->where('email', $request->data['email'])->first();
        $array = Arr::prepend($data[0], $id->id, 'id');

        if($request->data['position'] == '0'){
            $patient = new Patient($array);

            $patient->save();
        }
        else{
            $doctor = new Doctor($array);
            $doctor->save();
        }
    }

    public function updateUser(UserUpdateRequest $request) {

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        if($user->position == 1){
            $doctor = Doctor::find($user->id);
            $doctor->name = $request->name;
            $doctor->email = $request->email;
            $doctor->save();
        }
        else{
            $patient = Patient::find($user->id);
            $patient->name = $request->name;
            $patient->email = $request->email;
            $patient->save();
        }

        return response()->json($user);
    }

    public function deleteUser(Request $request){
        Patient::find($request->id)->delete();
        User::find($request->id)->delete();
        dd(User::find($request->id)->delete());
    }
}
