<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    protected $email = 'cruzywork@gmail.com';

    public function send()
    {
        $user = User::find(Auth::id());

        Mail::mailer('cruzywork@gmail.com')
            ->to($user)
            ->send(new TestMail('Dear ' . $user->name . '. You just made an appointment with a doctor.'));
        return redirect('/');
//        return (new MailMessage)
//            ->replyTo($this->email)
//            ->greeting('Hello, ' . $user->name . '!')
//            ->line('My name is vasya')
//            ->action('Press', 'https://www.google.com.ua/');
    }

}
