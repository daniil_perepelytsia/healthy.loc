<?php

namespace App\Http\Requests;

use App\Models\Hospital\Doctor;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

/**
 * Class PatientDoctorRequest
 * @package App\Http\Requests
 *
 * @property Patient $patient
 * @property Doctor $doctor
 * @property string $date
 * @property string $time
 */
class PatientDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doctor' => 'required|integer|exists:doctors,id',
            'datetime' => 'required|date|after_or_equal:today'
        ];
    }

//    /**
//     * Configure the validator instance.
//     *
//     * @param  Validator  $validator
//     * @return void
//     */
//    public function withValidator($validator)
//    {
//        $validator->after(function ($validator) {
//            $datetime = $this->date . ' ' . $this->time;
//            $val = strtotime($datetime);
//            if (!isset($val)) {
//                $validator->errors()->add('null', 'incorrect' . $datetime);
//            }
//        });
//    }

    protected function prepareForValidation()
    {
        $this->getInputSource()->add([
            'datetime' => $this->date . ' ' . $this->time,
        ]);
    }
}
