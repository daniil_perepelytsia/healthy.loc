<?php


namespace App\Repositories;


use App\Models\Hospital\Patient;
use App\Models\User;

class PatientRepository
{
    public function getPatients(){
        $patientsData = [];
        $patients = Patient::all();

        for ($i=0;$i<count($patients); $i++){
            $user = User::find($patients[$i]->user_id);
            $patientsData[$i] = [
                'id' => $user->id,
                'name' => $user->name
            ];
        }
        return $patientsData;
    }

    public function getPatientsName($patients){
        for ($i=0;$i<count($patients); $i++){
            $user = User::find($patients[$i]->user_id);
            $patientsData[$i] = [
                'id' => $user->id,
                'name' => $user->name
            ];
        }

        return $patientsData;
    }
}
