<?php


namespace App\Repositories;


use App\Models\Hospital\Doctor;
use App\Models\User;

class DoctorRepository
{
    public function getDoctors(){
        $doctorsData = [];
        $doctors = Doctor::all();

        for ($i=0;$i<count($doctors); $i++){
            $user = User::find($doctors[$i]->user_id);
            $doctorsData[$i] = [
                'id' => $user->id,
                'name' => $user->name,
                'specialization' => $user->specialization
            ];
        }
        return $doctorsData;
    }

    public function getDoctorsName($doctors){
        $doctor = User::find($doctors);

        for ($i=0;$i<count($doctors); $i++){
            $user = User::find($doctors[$i]->id);
            $doctorsData[$i] = [
                'id' => $user->id,
                'name' => $user->name
            ];
        }

        return $doctorsData;
    }
}
