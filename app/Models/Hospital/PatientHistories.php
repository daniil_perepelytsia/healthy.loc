<?php

namespace App\Models\Hospital;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Notifications\Notifiable;

class PatientHistories extends Pivot
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'patient_id',
        'doctor_id',
        'diagnosis',
        'treatment',
        'created_at',
        'updated_at'
    ];

    protected $table = 'patient_histories';
}
