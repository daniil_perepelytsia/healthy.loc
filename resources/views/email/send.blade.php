@component('mail::message')
# Hello

{{$text}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
