@extends('layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Home page') }}</div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-6">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <h3>ID: {{Auth::id()}}</h3>
                            <h3>{{Auth::user()->name}}</h3>
                            <h5 class="text-muted">{{Auth::user()->email}}</h5>
                        </div>

                        <div class="col-6" style="text-align: right">

                            <a href="{{route('add.doctor')}}" class="btn btn-success" style="margin-top: 10px">Add doctor &#10010;</a><br>
                            <a href="{{route('add.patient')}}" class="btn btn-primary" style="margin-top: 10px">Add patient ☺</a><br>
                            <a href="{{route('patient.make.appointment')}}" class="btn btn-outline-secondary" style="margin-top: 10px">Make appointment &#10000;</a><br>

{{--                            <a href="{{route('write.to.card')}}" class="btn btn-outline-success" style="margin-top: 10px">Write to the card</a>--}}
{{--                            <a href="{{route('patient.appointments')}}" class="btn btn-outline-secondary" style="margin-top: 10px">Appointments</a>--}}
{{--                            <a href="{{route('email.send')}}" class="btn btn-outline-success" style="margin-top: 10px">Send email</a>--}}
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
