
@extends('layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">
                    <div class="card-header" style="text-align: center">{{ __('Appointments list') }}</div>
                    <div class="card-body">
                        <div class="row">

                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <ul>
                                @if(count($appointment) > 0)
                                    @foreach($appointment as $app)
                                        <li style="list-style: none">
                                            <div class="row" style="width: 100%">
                                                <div class="col-12">
                                                    APP: {{$app->pivot->id}}

                                                    <h3> Doctor ID: {{$app->pivot->doctor_id}} </h3>
                                                    <h3> Specialization: {{$app->specialization}} </h3>
                                                    <h3> Appointment time: {{$app->pivot->appointment_time}} </h3>
                                                    <a href=" {{route('patient.cancel.appointment', [$app->pivot->patient_id, $app->pivot->id])}} "
                                                       class="btn btn-outline-secondary">Cancel appointment</a>
                                                </div>
                                            </div>
                                        </li>
                                        <hr>
                                    @endforeach
                                @else
                                   <h5>No appointments</h5>
                                @endif
{{--                                    <h2 class="text-muted">No appointments</h2>--}}
                            </ul>

                        </div>

                    </div>
                </div>
                <a href="{{route('patient.make.appointment')}}" class="btn btn-outline-primary" style="margin-top: 20px; width: 100%">Make appointment</a>
            </div>
        </div>
    </div>
@endsection
