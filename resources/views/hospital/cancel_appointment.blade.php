@extends('layout')

@section('content')
    <div class="container-full-width" style="text-align: center">
        Are you sure you want cancel this appointment?<br><br>
        <a href="{{route('patient.delete.appointment', [$patient_id, $id])}}" class="btn btn-outline-danger">Cancel appointment</a>
    </div>
@endsection
