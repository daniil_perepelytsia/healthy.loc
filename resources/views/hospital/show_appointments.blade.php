@extends('layout')

@section('content')
    <div class="container-full-width" style="height: 100%;"><br><br>
        <Appointments :id="{{$id}}"></Appointments>
        <br><br>
    </div>
@endsection

{{--<script>--}}
{{--    import Appointments from "../../js/components/Appointments";--}}
{{--    export default {--}}
{{--        components: {Appointments}--}}
{{--    }--}}
{{--</script>--}}
