
@extends('layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header" style="text-align: center">{{ __('Patients list') }}</div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-6">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <ul>
                                    @foreach($patients as $patient)
                                        <li style="list-style: none">
                                            <div class="row">

                                                <div class="col-4">
                                                    <p style="font-size: 13px"> Patient:  </p>
                                                    <p style="font-size: 13px"> Diagnosis: </p>
                                                    <p style="font-size: 13px"> Visit time: </p>
                                                </div>

                                                <div class="col-4">
                                                    <h5> {{$patient->name}} </h5>
                                                    <h5> {{$patient->pivot->diagnosis}} </h5>
                                                    <h5>{{$patient->pivot->updated_at}} </h5>
                                                </div>

                                                <div class="col-4" style="text-align: right">
                                                </div>

                                            </div>


                                        </li>
                                        <hr>
                                    @endforeach
                                </ul>
                            </div>

                        </div>

                    </div>
                    <a href="{{route('write.to.card')}}" class="btn btn-outline-success">Write to the card</a>

                </div>
            </div>
        </div>
    </div>
@endsection
