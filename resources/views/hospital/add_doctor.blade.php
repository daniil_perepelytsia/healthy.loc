@extends('layout')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($errors->any())
                        <div class="alert alert-danger" style="text-align: center">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-header">{{ __('Create a doctor') }}</div>

                    <div class="card-body">
                        <form action="{{route('save.doctor')}}" method="post">
                            @csrf
                            <div class="row">

                                <div class="col">
                                    Doctor
                                    <input type="text" name="name" placeholder="Enter name" class="form-control" style="margin-bottom: 10px">
                                    <input type="email" name="email" placeholder="Enter email" class="form-control" style="margin-bottom: 10px">

                                    <select name="specialization" class="form-control" style="margin-bottom: 10px">
                                        @foreach($specializations as $specialization)
                                            <option value="{{ $specialization }}">{{ $specialization }}</option>
                                        @endforeach
                                    </select>

                                    <input type="submit" class="btn btn-outline-success" style="width: 100%; margin-bottom: 10px" value="Save">
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
