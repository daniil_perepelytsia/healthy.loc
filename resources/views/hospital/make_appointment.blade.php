@extends('layout')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($errors->any())
                        <div class="alert alert-danger" style="text-align: center">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    <div class="card-header">{{ __('Make an appointment') }}</div>

                    <div class="card-body">
                        <form action="{{route('patient.save.appointment')}}" method="post">
                        @csrf
                            <div class="row">

                                <div class="col-6">
                                    Doctor
                                    <select name="doctor" class="form-control">
                                        @foreach($doctorsData as $doctor)
                                            <option value="{{$doctor['id']}}">{{$doctor['name']}}</option>
                                        @endforeach
                                    </select><br>


                                    Select patient:
                                    <select name="patient" class="form-control">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-muted">If you want to register a new patient, you can do this on the main page.</small>
                                    <br>
                                </div>

                                <div class="col-6" style="text-align: left">
                                    Date:
                                    <input  class="form-control" type="date" name="date" min="{{date('d:m:Y')}}"><br>
                                    Time:
                                    <input class="form-control" type="time" name="time"><br>
                                </div>

                                <input type="submit" class="btn btn-success" value="Sign in" style="width: 100%">

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
