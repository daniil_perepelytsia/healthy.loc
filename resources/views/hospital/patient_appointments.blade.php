
@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">{{ __('Appointments list') }}</div>

                <div class="card-body" style="text-align: left">
{{--                        <div class="col-6">--}}
{{--                            @if (session('status'))--}}
{{--                                <div class="alert alert-success" role="alert">--}}
{{--                                    {{ session('status') }}--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        </div>--}}
                            <ul>
                            @if(count($app) > 0)
                                @foreach($app as $patient)
                                    @if(count($patient->doctors) > 0)
                                        <li style="list-style: none">
                                            <div class="row" style="width: 100%;">
                                                <div class="col">
                                                    <small>Patient: </small>
                                                    <h4 style="margin-left: 1px"><a href="{{route('appointment', $patient->id)}}" style="text-transform: capitalize"> {{$patient->users->name}} </a></h4>
                                                    <br>
                                                    <a href="{{route('appointments.show', $patient->id)}}" class="btn btn-outline-primary" style="width: 100px">See all &#10148;</a><br>

                                                    <small>{{count($patient->doctors)}} appointment(s).</small>
                                                    <hr>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            @else
                                <h2 class="text-muted">No appointments</h2>
                            @endif
                            </ul>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                {{$app->links("pagination::bootstrap-4")}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <a href="{{route('patient.make.appointment')}}" class="btn btn-outline-primary" style="margin-top: 20px; width: 100%">Make appointment</a>
        </div>
    </div>
</div>
@endsection
