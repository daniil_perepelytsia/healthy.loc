@extends('layout')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($errors->any())
                        <div class="alert alert-danger" style="text-align: center">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-header">{{ __('Add a patient') }}</div>

                    <div class="card-body">
                        <form action="{{route('save.patient')}}" method="post">
                            @csrf
                            <div class="row">

                                <div class="col">
                                    Patient
                                    <input type="text" name="name" placeholder="Enter name" class="form-control" style="margin-bottom: 10px">
                                    <input type="email" name="email" placeholder="Enter email" class="form-control" style="margin-bottom: 10px">

                                    <input type="submit" class="btn btn-outline-success" style="width: 100%; margin-bottom: 10px" value="Save">

                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
