@extends('layout')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($errors->any())
                        <div class="alert alert-danger" style="text-align: center">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-header">{{ __('Write to card') }}</div>

                    <div class="card-body">
                        <form action="{{route('doctor.save.card')}}" method="post">
                            @csrf
                            <div class="row">

                                <div class="col-6">
                                    Doctor
                                    <input type="hidden" name="doctor" value="{{auth()->user()->id}}" class="form-control">
                                    <h4>{{auth()->user()->name}}</h4><br>
                                    Patient
                                    <select name="patient" class="form-control">
                                        @foreach($patients as $patient)
                                            <option value="{{$patient->id}}">{{$patient->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6" style="text-align: left">
                                    Diagnosis:
                                    <input  class="form-control" type="text" name="diagnosis" placeholder="Write diagnosis"><br>
                                    Treatment:
                                    <textarea class="form-control" name="treatment" placeholder="Write treatment" style="height: 200px"></textarea><br>
                                </div>

                                <input type="submit" class="btn btn-outline-success" value="Write to the card">

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
