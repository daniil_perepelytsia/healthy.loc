<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body>

<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('Healthy', 'Healthy') }}
            </a>


            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <div class="row" style="width: 1000px"></div>
                        <a href="{{route('users')}}" class="btn btn-outline-light" style="color: #1a202c">Test</a>
                        <a href="{{route('home')}}" class="btn btn-outline-light" style="color: #1a202c">Profile</a>
                        @if(auth()->user()->position == 0)
                        <a href="{{route('patient.appointments')}}" class="btn btn-outline-light" style="color: #1a202c">Appointments</a>
                        @else
                            <a href="{{route('patients')}}" class="btn btn-outline-light" style="color: #1a202c">Patients</a>
                        @endif
                        <a href="{{route('logout')}}" class="btn btn-outline-danger" >Logout</a>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>







{{--<nav class="navbar navbar-expand-lg navbar-dark bg-dark">--}}
{{--    <a href="/home" class="navbar-brand"><img src="https://icon-library.com/images/icon-blog-png/icon-blog-png-20.jpg" width="30px" height="30px" alt="logo"></a>--}}
{{--    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--        <span class="navbar-toggler-icon"></span>--}}
{{--    </button>--}}

{{--    <div class="collapse navbar-collapse">--}}
{{--        <ul class="navbar-nav mr-auto">--}}
{{--            <li class="nav-item active">--}}
{{--                <a href="/patient/userslist" class="nav-link">Patients</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="/doctor/userslist" class="nav-link">Doctor</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="/patient/profile/{{session('email')}}" class="nav-link">Profile</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="{{route('patient.registration')}}" class="nav-link">Registration</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</nav>--}}

{{--<div class="container-full-width">--}}
{{--    @yield('content')--}}
{{--</div>--}}


</body>
