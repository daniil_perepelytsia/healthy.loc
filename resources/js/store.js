import Vue from 'vue';
import Vuex from 'vuex';
import Vuelidate from "vuelidate/src";

Vue.use(Vuelidate)
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            id: 1,
            name: "Test Name"
        }
    },

    getters: {
        userId: (state) => state.user.id,
        userName: (state) => {
            return state.user.name;
        },
        userLoggedIn: (state, getters) => {
            return getters.userId > 0;
        },
        getNums: (state) => state.numbers
    },

    // mutations:{},

    actions: {


        loadTest() {
            return new Promise((resolve) => {
                setTimeout(() => {
                    resolve('ttreiwotr')
                }, 1000)
            })
        },



    }
});
